var express = require('express');
var app = express();
var fs = require("fs"); // Lee de disco, archivos
var http = require("http").Server(app);
var io = require('socket.io')(http);
var jsonfile = require('jsonfile'); // Lee y escribe en archivos planos JSON

// Archivo con comentarios
var fileJsonCommentsPath = './public/comentarios.json'; 
var fileJsonComments = require(fileJsonCommentsPath); 


app.use(express.static('public'));

app.get('/api/comments', function(req, res){
	fs.readFile(fileJsonCommentsPath, 'utf8', function (err, data) {
	  if (err) throw err;
	  var obj = JSON.parse(data); 
  	  res.json(obj);
	});
});

// Implementación Socket.IO
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});
io.on('connection', function(socket){
  socket.on('nuevo_msg', function(msg){
    console.log('author: ' + msg.author + ' | msg: ' +msg.message);
    	jsonfile.readFile(fileJsonCommentsPath, function(err, obj) {
          obj.push({
          	id: msg.id, 
          	author: msg.author, 
          	text: msg.message
          });
        // Escribe archivo mientras bloquea ejecución (sync)
    	  jsonfile.writeFileSync(fileJsonCommentsPath, obj, {spaces:2}, function(err) {
    	  	console.error(err)
    	  });
        // Lee comments de disco
    	  res.json(fileJsonComments); 
    	});	
  });
});

// Esto levanta la app pero no levanta el SocketIO Server:3000
// app.listen(3000, function () {
//   	console.log('Example app listening on port 3000!');
// });

http.listen(3000, function(){
  console.log('listening on *:3000');
});