# README

App React de ejemplo, siguiendo: [https://facebook.github.io/react/docs/tutorial.html](https://facebook.github.io/react/docs/tutorial.html)
y luego [Socket.IO](http://socket.io/get-started/chat/)

App se levanta con comando "npm start", en puerto 3000

Abrir http://localhost:3000/index.html

## Cómo instalar app

Se debe descargar el repo a una carpeta sobre la cual se ddebe correr el comando "npm install", y luego el "npm start" si es que se instalaron bien los paquetes de la app.